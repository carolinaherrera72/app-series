import React from 'react';
import {render} from '@testing-library/react';
import App from './App';
import {prettyDOM} from "@testing-library/react";

test('renders content', () => {
    const component = render(<App/>)
    component.getAllByText('Volver a inicio')
    component.debug()
    const a = component.container.querySelector('a')
    console.log(prettyDOM(a))
});
