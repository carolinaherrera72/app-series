import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";
i18n
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        resources: {
            en: {
                translations: {
                    "PERSONAJES DE SERIES": "SERIES CHARACTERS",
                    "Listado en microfrontend": "List in microfronted",
                    "Volver a inicio": "Go to home",
                    "Especie": "Specie",
                    "Genero": "Gender",
                    "Casa": "House",
                    "Color de ojos": "Eye colour",
                    "human": "Human",
                    "cat": "Cat",
                    "male": "male",
                    "female": "female",

                }
            },
            es: {
                translations: {
                    "PERSONAJES DE SERIES": "PERSONAJES DE SERIES",
                    "Listado en microfrontend": "Listado en microfrontend",
                    "Volver a inicio": "Volver a inicio",
                    "Especie": "Especie",
                    "Genero": "Genero",
                    "Casa": "Casa",
                    "Color de ojos": "Color de ojos",
                    "human": "Humano",
                    "cat": "Gato",
                    "male": "Masculino",
                    "female": "Femenino",



                }
            }
        },
        fallbackLng: "es",
        debug: true,
        ns: ["translations"],
        defaultNS: "translations",
        keySeparator: false,
        interpolation: {
            escapeValue: false
        }
    }).then( r =>'');
export default i18n;
