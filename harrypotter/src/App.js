import React, {useEffect, useState} from 'react';
import styles from "./Api.module.css"
import {Trans, useTranslation} from "react-i18next";

function App() {
  const [harry, setHarry] = useState([])
  const url = 'http://hp-api.herokuapp.com/api/characters'
  const { t } = useTranslation();

  useEffect(() => {
    console.log(localStorage.getItem('lang'))
      fetch(url)
        .then((response) => {
          return response.json()
        })
        .then((harry) => {
          setHarry(harry)
        })
  }, [])

  return (
      <div>
          <div className={styles.container}>
                  <a className={styles.ahome} href='/home'>Volver a inicio</a>
          </div>
          <h1 className={styles.container__tittle}>
              Harry Potter
          </h1>
          <div className={styles.card}>

              {harry.map(harry => {
                  return harry.image && (
                      <div key={harry.image} >
                          <div className={styles.card__container} >
                              <p className={styles.card__name}><strong className={styles.card__str}>{harry.name}</strong></p>
                              <div className={styles.card__imgContent}>
                                  <img
                                      className={styles.card__image}
                                      width={150}
                                      src={harry.image}
                                      alt={harry.name}/>
                              </div>

                              <div className={styles.card__info}>
                                  <p className={styles.card__ifoItem}><strong className={styles.card__str}>{t("Especie")} </strong>
                                      <Trans>
                                      {harry.species}
                                  </Trans>
                                  </p>
                                  <p className={styles.card__ifoItem}><strong className={styles.card__str}>{t("Genero")} </strong>
                                      <Trans>
                                          {harry.gender}
                                      </Trans>
                                      </p>
                                  <p className={styles.card__ifoItem}><strong className={styles.card__str}>{t("Casa")}  </strong>{harry.house}</p>

                          </div>

                      </div>
              </div>
              );
              })}
          </div>
      </div>

  );

}

export default App;

