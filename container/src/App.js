import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import MicroFrontend from "./MicroFrontend";
import styles from "./App.module.css";
import {useTranslation, Trans} from "react-i18next";
import Home from './Home'
const {
    REACT_APP_SUBAPP1_HOST: subapp1,
    REACT_APP_SUBAPP2_HOST: subapp2
} = process.env;

const SubApp2 = ({history}) => (
    <MicroFrontend history={history} host={subapp2} name="subapp2"/>
);

const SubApp1 = ({history}) => (
    <MicroFrontend history={history} host={subapp1} name="subapp1"/>
);

const App = props => {
    const {t, i18n} = useTranslation();

    const changeLanguage = lng => {
        i18n.changeLanguage(lng).then(r => window.location.reload(false));
    };
    return (
        <div>
            <header className={styles.nav}>
                <a href="/home">
                    <h1 className={styles.nav__title}>
                        <Trans>
                            PERSONAJES DE SERIES
                        </Trans>
                    </h1>
                </a>
                <h2 className={styles.h2__tittle}>{t("Listado en microfrontend")}</h2>
            </header>
            <div className={styles.nav__button}>
                <button  className={styles.nav__buttonEs}  onClick={() => changeLanguage("es")}>es</button>
                <button  className={styles.nav__buttonEn}  onClick={() => changeLanguage("en")}>en</button>
            </div>

            <BrowserRouter>
                <Switch>
                    <Route path="/home" component={Home}/>
                    <Route path="/harrypotter" render={() => <SubApp1/>}/>
                    <Route path="/rickandmorty" render={() => <SubApp2/>}/>
                </Switch>
            </BrowserRouter>
        </div>
    );
};

export default App;
