import React from "react";
import styles from "./App.module.css";
import {useTranslation} from "react-i18next";
const Home = props => {
    const {t} = useTranslation();

    return (
        <>
            <div className={styles.container}>
                <div className={styles.container__home}>
                    <div className={styles.container__per}>
                        <div className={styles.container__card}>
                            <a href="/rickandmorty" className={styles.container__a}>
                                <img className={styles.container__image} alt={"rick and morty image"}
                                     src={"https://sm.ign.com/ign_es/screenshot/default/blob_qcqn.jpg"}/>
                                <h2 className={styles.container__title}>{t("RICK Y MORTY")}</h2>

                            </a>
                        </div>
                    </div>

                    <div className={styles.container__per}>
                        <div className={styles.container__card}>
                            <a href="/harrypotter" className={styles.container__a}>
                                <img className={styles.container__image} alt={"harry potter image"}
                                     src={"https://monterreylive.com/wp-content/uploads/2021/01/harry-potter-TV-Series.jpg"}/>
                                <h2 className={styles.container__title}>HARRY POTTER</h2>
                            </a>
                        </div>

                    </div>
                </div>


            </div>
        </>

    );
};

export default Home;