import React from 'react';
import {render, screen} from '@testing-library/react';

import App from "./App";

describe("App", () => {
  let originalFetch;
  beforeEach(() => {
    originalFetch = global.fetch;
    global.fetch = jest.fn(() => Promise.resolve({
      json: () => Promise.resolve({
        value: "Volver a inicio"
      })
    }));
  });
  afterEach(() => {
    global.fetch = originalFetch;
  });
  it('Should have proper description after data fetch', async () => {
   render(<App />);
    const description = await screen.findByTestId('description');
    expect(description.textContent).toBe("Volver a inicio");
  });
});