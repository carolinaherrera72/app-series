import React, {useEffect, useState} from 'react';
import styles from "./App.module.css"
import axios from "axios";
import CharacterList from "./components/CharacterList";
import { useTranslation,  } from "react-i18next";
function App() {
    const [characters, setCharacters] = useState([]);
    const url = "https://rickandmortyapi.com/api/character";
    const { t } = useTranslation();
    const fetchCharacters = (url) => {
        axios
            .get(url)
            .then((data) => {
                setCharacters(data.data.results);
            })
            .catch((error) => {
                console.log(error);
            });
    };
    useEffect(() => {
        fetchCharacters(url);
    }, []);
  return (
      <div>
          <div className={styles.container}>
                  <a className={styles.container__buttonlink} href='/home'>{t("Volver a inicio")}</a>
          </div>
          <div>
              <h1 className={styles.container__tittle}>{t("RICK Y MORTY")}</h1>
          </div>
          <CharacterList characters={characters} />
      </div>

  );
}
export default App;
