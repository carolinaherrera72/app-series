import React from 'react';
import {Trans, useTranslation,} from "react-i18next";
import styles from "./Api.module.css"

const CharacterList = ({characters}) => {
    const {t} = useTranslation();
    return (
        <div className={styles.card}>
            {characters.map((item, index) => (<div className={styles.container} key={index}>
                <div>
                    <div className={styles.back}>
                        <strong><p className={styles.card__name}>{item.name}</p></strong>
                        <figure className={styles.card__imgContent}>
                            <img
                                className={styles.card__image}
                                width={150}
                                src={item.image}
                                alt="character"/>
                        </figure>
                    </div>
                    <div className={styles.card__backInfo}>
                        <div className={styles.card__backInfo}>

                            <div className={styles.card__info}>
                                <p><strong>{t("Especie")} </strong>
                                    <Trans>
                                        {item.species}
                                    </Trans>
                                    </p>
                                <p><strong>{t("Estado")} </strong>
                                    <Trans>
                                        {item.status}
                                    </Trans>
                                    </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>))}
        </div>
    );
};

export default CharacterList;


