import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";
i18n
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        resources: {
            en: {
                translations: {
                    "PERSONAJES DE SERIES": "SERIES CHARACTERS",
                    "Listado en microfrontend": "List in microfronted",
                    "RICK Y MORTY": "RICK AND MORTY",
                    "Volver a inicio":"Go to home",
                    "Especie":"Species",
                    "Estado":"Status",
                    "Human":"Human",
                    "Alive":"Alive",
                    "Dead":"Dead",
                    "unknown":"unknown",


                }
            },
            es: {
                translations: {
                    "PERSONAJES DE SERIES": "PERSONAJES DE SERIES",
                    "Listado en microfrontend": "Listado en microfrontend",
                    "RICK Y MORTY": "RICK Y MORTY",
                    "Volver a inicio":"Volver a inicio",
                    "Especie":"Especie",
                    "Estado":"Estado",
                    "Human":"Humano",
                    "Alive":"Vivo",
                    "Dead":"Muerto",
                    "unknown":"Desconocido",

                }
            }
        },
        fallbackLng: "es",
        debug: true,
        ns: ["translations"],
        defaultNS: "translations",
        keySeparator: false,
        interpolation: {
            escapeValue: false
        }
    }).then( r =>'');
export default i18n;
